﻿//ссылка на библиотеку классов.
using System;

//Пространсво имён.
namespace ConsoleSettings
{
    //Класс программы.
    class Programm
    {

        //Метод Main - точка начала выполнения программы.
        static void Main(string[] args)
        {
            Console.Title = "Информационое табло";
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Информация о системе:");

            Console.WriteLine(Environment.MachineName);
            Console.WriteLine(Environment.OSVersion);
            Console.WriteLine(Environment.UserDomainName);
            Console.WriteLine(Environment.UserName);
            Console.WriteLine(Environment.Version);
            //Console.WriteLine(Environment.CurrntDirectory);
            //Console.WriteLine(Environment.ProcessId);
            Console.WriteLine(Environment.ProcessorCount);
            Console.WriteLine(Environment.Is64BitProcess);
            Console.WriteLine(Environment.Is64BitOperatingSystem);

            //Выведет пустую строку. Позволяет нам визуально отделить текст в консоли.
            Console.WriteLine();
            //Gпросим пользователя ввести в строку с помощью команды ConsoleWriteLine() и сразу отобразим ее на экране.
            Console.WriteLine(Console.ReadLine());
        }
    }
}



















